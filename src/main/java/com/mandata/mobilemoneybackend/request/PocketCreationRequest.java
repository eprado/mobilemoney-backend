package com.mandata.mobilemoneybackend.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PocketCreationRequest extends TransactionRequest {
	
	@NotNull
	@NotBlank
	private String pocketName;
	
	@NotNull
	private BigDecimal pocketBalance;

	public PocketCreationRequest() {
		super();
	}
	
	public PocketCreationRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion, String pocketName, BigDecimal pocketBalance) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.pocketName = pocketName;
		this.pocketBalance = pocketBalance;
	}



	public String getPocketName() {
		return pocketName;
	}

	public void setPocketName(String pocketName) {
		this.pocketName = pocketName;
	}

	public BigDecimal getPocketBalance() {
		return pocketBalance;
	}

	public void setPocketBalance(BigDecimal pocketBalance) {
		this.pocketBalance = pocketBalance;
	}

	@Override
	public String toString() {
		return super.toString() + "-->PocketRequest [pocketName=" + pocketName + ", pocketBalance=" + pocketBalance + "]";
	}

	
	

}

