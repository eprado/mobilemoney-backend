package com.mandata.mobilemoneybackend.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class WithdrawalRequest extends TransactionRequest{
	
	private BigDecimal withdrawalAmount;

	
	
	public WithdrawalRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WithdrawalRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion, BigDecimal withdrawalAmount) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.withdrawalAmount = withdrawalAmount;
	}

	public BigDecimal getWithdrawalAmount() {
		return withdrawalAmount;
	}

	public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	@Override
	public String toString() {
		return super.toString() + "-->WithdrawalRequest [withdrawalAmount=" + withdrawalAmount + "]";
	}
	
	
	

}
