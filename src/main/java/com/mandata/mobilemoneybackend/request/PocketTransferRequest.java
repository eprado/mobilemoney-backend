package com.mandata.mobilemoneybackend.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PocketTransferRequest extends TransactionRequest{

	private int pocketId;
	
	private int pocketTargetId;
	
	private BigDecimal transferAmount;

	public PocketTransferRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PocketTransferRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion, int pocketId, int pocketTargetId, BigDecimal transferAmount) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.pocketId = pocketId;
		this.pocketTargetId = pocketTargetId;
		this.transferAmount = transferAmount;
	}

	public int getPocketId() {
		return pocketId;
	}

	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}

	public int getPocketTargetId() {
		return pocketTargetId;
	}

	public void setPocketTargetId(int pocketTargetId) {
		this.pocketTargetId = pocketTargetId;
	}

	public BigDecimal getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	@Override
	public String toString() {
		return super.toString() + "-->PocketTransferRequest [pocketId=" + pocketId + ", pocketTargetId=" + pocketTargetId
				+ ", transferAmount=" + transferAmount + "]";
	}
	
	

}
