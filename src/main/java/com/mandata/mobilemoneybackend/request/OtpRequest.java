package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class OtpRequest extends ServiceRequest {

	@NotNull
	@NotBlank
	private String phoneNumber;
	

	public OtpRequest() {
		super();
	}

	public OtpRequest(int bankCod, String canal, String phoneNumber) {
		super(bankCod, canal);
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return super.toString() +  " OtpRequest [phoneNumber=" + phoneNumber + "]";
	}
	
}
