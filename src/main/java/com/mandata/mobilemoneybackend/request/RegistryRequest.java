package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegistryRequest extends TransactionRequest {


	@NotNull
	@NotBlank
	private String idType;

	@NotNull
	@NotBlank
	private String idNumber;

	private String firstName;

	private String secondName;

	private String firstLastName;

	private String secondLastName;

	private String email;

	private String imei;

	private String idExpeditionDate;

	private String accept1;

	private String accept2;

	
	@NotNull
	@NotBlank
	@Size(min=4, max=4)
	private String password;
	
	
	public RegistryRequest() {
		super();
	}



	public RegistryRequest(@NotNull @NotBlank String idType, @NotNull @NotBlank String idNumber,
			String firstName, String secondName, String firstLastName, String secondLastName, String email, String imei,
			String idExpeditionDate, String accept1, String accept2,
			@NotNull @NotBlank @Size(min = 4, max = 4) String password, @NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.idType = idType;
		this.idNumber = idNumber;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.email = email;
		this.imei = imei;
		this.idExpeditionDate = idExpeditionDate;
		this.accept1 = accept1;
		this.accept2 = accept2;
		this.password = password;
	}



	public String getIdType() {
		return idType;
	}


	public void setIdType(String idType) {
		this.idType = idType;
	}


	public String getIdNumber() {
		return idNumber;
	}


	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getSecondName() {
		return secondName;
	}


	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}


	public String getFirstLastName() {
		return firstLastName;
	}


	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}


	public String getSecondLastName() {
		return secondLastName;
	}


	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getImei() {
		return imei;
	}


	public void setImei(String imei) {
		this.imei = imei;
	}


	public String getIdExpeditionDate() {
		return idExpeditionDate;
	}


	public void setIdExpeditionDate(String idExpeditionDate) {
		this.idExpeditionDate = idExpeditionDate;
	}


	public String getAccept1() {
		return accept1;
	}


	public void setAccept1(String accept1) {
		this.accept1 = accept1;
	}


	public String getAccept2() {
		return accept2;
	}


	public void setAccept2(String accept2) {
		this.accept2 = accept2;
	}


	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}



	@Override
	public String toString() {
		return super.toString() + "-->RegistryRequest [idType=" + idType + ", idNumber=" + idNumber + ", firstName=" + firstName
				+ ", secondName=" + secondName + ", firstLastName=" + firstLastName + ", secondLastName="
				+ secondLastName + ", email=" + email + ", imei=" + imei + ", idExpeditionDate=" + idExpeditionDate
				+ ", accept1=" + accept1 + ", accept2=" + accept2 + ", password=" + password + "]";
	}

}
