package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotNull;

public class ServiceRequest {
	
	
	@NotNull
	private int bankCod;
	
	private String canal;
	

	public ServiceRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ServiceRequest(int bankCod, String canal) {
		super();
		this.bankCod = bankCod;
		this.canal = canal;
	}

	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}


	@Override
	public String toString() {
		return "ServiceRequest [bankCod=" + bankCod + ", canal=" + canal + "]";
	}
	
	
	
	
}
