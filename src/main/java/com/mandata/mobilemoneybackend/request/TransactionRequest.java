package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class TransactionRequest extends ServiceRequest{
	
	@NotNull
	private long bin;
	
	@NotNull
	private int countryCod;
	
	@NotNull
	@NotBlank
	private String phoneNumber;
	
	private String appVersion;
	

	public TransactionRequest() {
		super();
	}


	public TransactionRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion) {
		super();
		this.bin = bin;
		this.countryCod = countryCod;
		this.phoneNumber = phoneNumber;
		this.appVersion = appVersion;
	}


	public long getBin() {
		return bin;
	}


	public void setBin(long bin) {
		this.bin = bin;
	}


	public int getCountryCod() {
		return countryCod;
	}


	public void setCountryCod(int countryCod) {
		this.countryCod = countryCod;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getAppVersion() {
		return appVersion;
	}


	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}


	@Override
	public String toString() {
		return "TransactionRequest [bin=" + bin + ", countryCod=" + countryCod + ", phoneNumber=" + phoneNumber
				+ ", appVersion=" + appVersion + "]";
	}
	
	
	

}
