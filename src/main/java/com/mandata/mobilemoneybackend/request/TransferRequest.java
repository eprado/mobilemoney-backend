package com.mandata.mobilemoneybackend.request;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class TransferRequest extends TransactionRequest {

	@NotNull
	private int countryCodTargetAccount;

	@NotNull
	@NotBlank
	private String phoneNumberTargetAccount;

	@NotNull
	private BigDecimal transferAmount;

	public TransferRequest() {
		super();
	}

	public TransferRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion, @NotNull int countryCodTargetAccount, @NotNull @NotBlank String phoneNumberTargetAccount,
			@NotNull BigDecimal transferAmount) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.countryCodTargetAccount = countryCodTargetAccount;
		this.phoneNumberTargetAccount = phoneNumberTargetAccount;
		this.transferAmount = transferAmount;
	}

	public int getCountryCodTargetAccount() {
		return countryCodTargetAccount;
	}

	public void setCountryCodTargetAccount(int countryCodTargetAccount) {
		this.countryCodTargetAccount = countryCodTargetAccount;
	}

	public String getPhoneNumberTargetAccount() {
		return phoneNumberTargetAccount;
	}

	public void setPhoneNumberTargetAccount(String phoneNumberTargetAccount) {
		this.phoneNumberTargetAccount = phoneNumberTargetAccount;
	}

	public BigDecimal getTransferAmount() {
		return transferAmount;
	}

	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}

	@Override
	public String toString() {
		return super.toString() + "-->TransferRequest [countryCodTargetAccount=" + countryCodTargetAccount
				+ ", phoneNumberTargetAccount=" + phoneNumberTargetAccount + ", transferAmount=" + transferAmount + "]";
	}

}
