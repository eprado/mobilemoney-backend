package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginRequest extends ServiceRequest {
	
	@NotNull
	private long bin;
	
	@NotNull
	private int countryCod;
	
	private String appVersion;
	
	@NotNull
	@NotBlank
	private String idType;

	@NotNull
	@NotBlank
	private String idNumber;
	
	@NotNull
	@NotBlank
	@Size(min=4, max=4)
	private String password;

	public LoginRequest() {
		super();
	}

	public LoginRequest(@NotNull long bin, @NotNull int countryCod, String appVersion, @NotNull @NotBlank String idType,
			@NotNull @NotBlank String idNumber, @NotNull @NotBlank @Size(min = 4, max = 4) String password) {
		super();
		this.bin = bin;
		this.countryCod = countryCod;
		this.appVersion = appVersion;
		this.idType = idType;
		this.idNumber = idNumber;
		this.password = password;
	}

	public long getBin() {
		return bin;
	}

	public void setBin(long bin) {
		this.bin = bin;
	}

	public int getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(int countryCod) {
		this.countryCod = countryCod;
	}

	public String getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(String appVersion) {
		this.appVersion = appVersion;
	}

	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return super.toString() + "-->LoginRequest [bin=" + bin + ", countryCod=" + countryCod + ", appVersion=" + appVersion + ", idType="
				+ idType + ", idNumber=" + idNumber + ", password=" + password + "]";
	}


}
