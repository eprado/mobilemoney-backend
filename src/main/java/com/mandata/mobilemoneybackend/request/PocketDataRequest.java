package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PocketDataRequest extends TransactionRequest{
	
	@NotNull
	private int pocketId;
	
	@Size(min=0, max=15)
	private String pocketName;

	public PocketDataRequest() {
		super();
	}

	public PocketDataRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion, @NotNull int pocketId, String pocketName) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.pocketId = pocketId;
		this.pocketName = pocketName;
	}

	public int getPocketId() {
		return pocketId;
	}

	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}

	public String getPocketName() {
		return pocketName;
	}

	public void setPocketName(String pocketName) {
		this.pocketName = pocketName;
	}

	@Override
	public String toString() {
		return super.toString() + "-->PocketModificationRequest [pocketId=" + pocketId + ", pocketName=" + pocketName + "]";
	}



	

}
