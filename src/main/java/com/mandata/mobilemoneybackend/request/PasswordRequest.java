package com.mandata.mobilemoneybackend.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class PasswordRequest extends TransactionRequest{
	
	private String currentPassword;
	
	private String newPassword;
	
	

	public PasswordRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PasswordRequest(@NotNull long bin, @NotNull int countryCod, @NotNull @NotBlank String phoneNumber,
			String appVersion, String currentPassword, String newPassword) {
		super(bin, countryCod, phoneNumber, appVersion);
		this.currentPassword = currentPassword;
		this.newPassword = newPassword;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Override
	public String toString() {
		return super.toString() + "-->PasswordRequest [currentPassword=" + currentPassword + ", newPassword=" + newPassword + "]";
	}

}
