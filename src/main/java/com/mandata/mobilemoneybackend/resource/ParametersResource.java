package com.mandata.mobilemoneybackend.resource;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Parameters;
import com.mandata.mobilemoneybackend.exception.ResourceNotFoundException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.request.ServiceRequest;
import com.mandata.mobilemoneybackend.response.ParametersResponse;
import com.mandata.mobilemoneybackend.service.ParametersRepository;

@RestController
public class ParametersResource {
	
	@Autowired
	private ParametersRepository service;
	
	@Autowired
	private LogRegister logRegister;
	
	
	@GetMapping("/parameters/{bankCod}/{canal}")
	public ParametersResponse retrieveByBankCod(@Valid ServiceRequest serviceRequest) throws Exception {
		
		//Registrar log de entrada
		logRegister.logIn(serviceRequest);
		
		//Consultar Parámetros
		List<Parameters> parameters = service.findByBankAndCanal(serviceRequest.getBankCod(), serviceRequest.getCanal());
		
		if(parameters.isEmpty())
			throw new ResourceNotFoundException("bankCod: " + serviceRequest.getBankCod() + " - " + "canal: " + serviceRequest.getCanal());
		
		ModelMapper modelMapper = new ModelMapper();
		
		ParametersResponse parametersResponse = new ParametersResponse();

		parametersResponse = modelMapper.map(parameters.get(0), ParametersResponse.class);
		parametersResponse.setReponseCod(HttpStatus.OK.toString());
		parametersResponse.setResponseDesc(HttpStatus.OK.name());
		
		//Registrar lod de salida
		logRegister.logOut(serviceRequest, parametersResponse);
		
		return parametersResponse;
			
	}

}
