package com.mandata.mobilemoneybackend.resource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Pocket;
import com.mandata.mobilemoneybackend.entity.TransactionalLog;
import com.mandata.mobilemoneybackend.exception.ResourceNotFoundException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.log.TransactionalLogRegister;
import com.mandata.mobilemoneybackend.model.TransactionCod;
import com.mandata.mobilemoneybackend.request.PocketDataRequest;
import com.mandata.mobilemoneybackend.request.TransactionRequest;
import com.mandata.mobilemoneybackend.response.BalanceResponse;
import com.mandata.mobilemoneybackend.response.MovementResponse;
import com.mandata.mobilemoneybackend.response.MyPocketsResponse;
import com.mandata.mobilemoneybackend.response.PocketResponse;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.PocketRepository;
import com.mandata.mobilemoneybackend.service.TransactionalLogRepository;

@RestController
public class ConsultResource {
	
	@Autowired
	private PocketRepository repository;

	@Autowired
	private TransactionalLogRepository transactionalLogRepository;
	
	@Autowired
	private LogRegister logRegister;

	@Autowired
	private TransactionalLogRegister transactionalLogRegister;

	
	@GetMapping("/balance/{bankCod}/{canal}/{bin}/{countryCod}/{phoneNumber}")
	public BalanceResponse getBalance(@Valid TransactionRequest request) {

		//Registrar Log de entrada
		logRegister.logIn(request);

		//Consultar Lista de Bolsillos
		List<Pocket> pocketList = repository.findPocketList(request.getBankCod(), request.getBin(),	request.getPhoneNumber());

		BigDecimal totalBalance = new BigDecimal(0);

		PocketResponse pocketResponse = new PocketResponse();

		for (Pocket pocket : pocketList) {

			BigDecimal pocketBalance = pocket.getBalance().getBalance();
			BigDecimal pocketDebit = pocket.getBalance().getDebitAmount();
			BigDecimal pocketCredit = pocket.getBalance().getCreditAmount();
			BigDecimal pocketPawned = pocket.getBalance().getPawnedAmount();
			
			//Calcular el Saldo de cada Bolsillo
			pocketBalance = pocketBalance.add(pocketDebit).subtract(pocketCredit).subtract(pocketPawned);

			if (pocket.getIsDefault().equals("S")) {
				pocketResponse.setPocketId(pocket.getPocketId());
				pocketResponse.setPocketName(pocket.getPocketName());
				pocketResponse.setPocketState(pocket.getPocketState());
				pocketResponse.setPocketBalance(pocketBalance);
			}

			//Calcular el Saldo total
			totalBalance = totalBalance.add(pocketBalance);
		}

		BalanceResponse response = new BalanceResponse(pocketResponse, totalBalance, HttpStatus.OK.toString(), HttpStatus.OK.name());

		transactionalLogRegister.log(request, response, pocketResponse.getPocketId(), TransactionCod.BALANCE_TRX.getCode(), new BigDecimal(0));
		logRegister.logOut(request, response);

		return response;
	}
	
	
	@GetMapping("/movements/{bankCod}/{canal}/{bin}/{countryCod}/{phoneNumber}/{pocketId}")
	public MovementResponse movements (@Valid PocketDataRequest request) {
		
		//Registrar Log de entrada
		logRegister.logIn(request);
		
		//Buscar movimientos
		List<TransactionalLog> movements = transactionalLogRepository.findByPocket(request.getBankCod(), request.getBin(), request.getCountryCod(), request.getPhoneNumber(), request.getPocketId());
		
		if(movements.isEmpty()) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("There are no movements for this pocket");
		}
		
		//Consultar Lista de Bolsillos
		List<Pocket> pocketList = repository.findPocketList(request.getBankCod(), request.getBin(),	request.getPhoneNumber());

		BigDecimal totalBalance = new BigDecimal(0);
		BigDecimal pocketTotalBalance = new BigDecimal(0);
		String pocketState = "";

		for (Pocket pocket : pocketList) {

			BigDecimal pocketBalance = pocket.getBalance().getBalance();
			BigDecimal pocketDebit = pocket.getBalance().getDebitAmount();
			BigDecimal pocketCredit = pocket.getBalance().getCreditAmount();
			BigDecimal pocketPawned = pocket.getBalance().getPawnedAmount();
			
			//Calcular el Saldo de cada Bolsillo
			pocketBalance = pocketBalance.add(pocketDebit).subtract(pocketCredit).subtract(pocketPawned);

			if (pocket.getPocketId()==request.getPocketId()) {
				pocketTotalBalance = pocketBalance;
				pocketState = pocket.getPocketState();
			}
			//Calcular el Saldo total
			totalBalance = totalBalance.add(pocketBalance);
		}
		
		MovementResponse response = new MovementResponse(totalBalance, pocketTotalBalance, pocketState, HttpStatus.OK.toString(), HttpStatus.OK.name(), movements);

		transactionalLogRegister.log(request, response, request.getPocketId(), TransactionCod.MOVEMENTS_TRX.getCode(), new BigDecimal(0));
		logRegister.logOut(request, response);
		
		return response;
	}

	
	
	
	@GetMapping("/mypockets/{bankCod}/{canal}/{bin}/{countryCod}/{phoneNumber}")
	public MyPocketsResponse myPockets (@Valid TransactionRequest request) {
		
		//Registrar Log de entrada
		logRegister.logIn(request);
		
		//Consultar Lista de Bolsillos
		List<Pocket> pocketList = repository.findPocketList(request.getBankCod(), request.getBin(), request.getPhoneNumber());

		List<PocketResponse> pocketResponseList = new ArrayList<>();

		for (Pocket pocket : pocketList) {

			BigDecimal pocketBalance = pocket.getBalance().getBalance();
			BigDecimal pocketDebit = pocket.getBalance().getDebitAmount();
			BigDecimal pocketCredit = pocket.getBalance().getCreditAmount();
			BigDecimal pocketPawned = pocket.getBalance().getPawnedAmount();

			//Calcular Saldo de Cada Bolsillo
			pocketBalance = pocketBalance.add(pocketDebit).subtract(pocketCredit).subtract(pocketPawned);

			PocketResponse pocketResponse = new PocketResponse(pocket.getPocketId(),pocket.getPocketName(),pocket.getPocketState(), pocketBalance);

			pocketResponseList.add(pocketResponse);
		}
		
		MyPocketsResponse response = new MyPocketsResponse(pocketResponseList, HttpStatus.OK.toString(), HttpStatus.OK.name());
		
		transactionalLogRegister.log(request, response, 0, TransactionCod.REGISTRY_TRX.getCode(), new BigDecimal(0));
		logRegister.logOut(request, response);
		
		return response;
	}

}
