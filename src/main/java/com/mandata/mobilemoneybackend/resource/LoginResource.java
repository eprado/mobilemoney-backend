package com.mandata.mobilemoneybackend.resource;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Master;
import com.mandata.mobilemoneybackend.entity.Pocket;
import com.mandata.mobilemoneybackend.exception.ResourceNotFoundException;
import com.mandata.mobilemoneybackend.exception.UnauthorizedException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.log.TransactionalLogRegister;
import com.mandata.mobilemoneybackend.model.TransactionCod;
import com.mandata.mobilemoneybackend.request.LoginRequest;
import com.mandata.mobilemoneybackend.response.LoginResponse;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.MasterRepository;
import com.mandata.mobilemoneybackend.service.PocketRepository;

@RestController
public class LoginResource {

	@Autowired
	private MasterRepository masterRepository;

	@Autowired
	private PocketRepository pocketRepository;

	@Autowired
	private LogRegister logRegister;

	@Autowired
	private TransactionalLogRegister transactionalLogRegister;

	
	@PostMapping("/login")
	public LoginResponse login(@Valid @RequestBody LoginRequest loginRequest) {

		//Registra log de entrada
		logRegister.logIn(loginRequest);

		//Consulta maestro por tipo y número de identificación
		Master master = masterRepository.findByIdentification(loginRequest.getBankCod(), loginRequest.getBin(),
				loginRequest.getCountryCod(), loginRequest.getIdType(), loginRequest.getIdNumber());

		//Valida si no existen los datos de la cuenta
		if (master == null) {
			logRegister.logOut(loginRequest, new ServiceResponse(HttpStatus.NOT_FOUND.toString(),HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException(
					"IdType: " + loginRequest.getIdType() + " - IdNumber:" + loginRequest.getIdNumber());
		}

		//Verifica clave
		if (!loginRequest.getPassword().equals(master.getPassword().trim())) {
			logRegister.logOut(loginRequest, new ServiceResponse(HttpStatus.UNAUTHORIZED.toString(),HttpStatus.UNAUTHORIZED.name()));
			throw new UnauthorizedException("Invalid Password: IdType: " + loginRequest.getIdType() + " - IdNumber:"
					+ loginRequest.getIdNumber());
		}
		
		//Consulta Bolsillo Default
		Pocket pocketDefault = pocketRepository.findByPrimaryKey(loginRequest.getBankCod(), loginRequest.getBin(), 1,
				master.getPhoneNumber());

		LoginResponse response = new LoginResponse(loginRequest.getBankCod(), pocketDefault.getPocketName(),
				pocketDefault.getPocketState(), pocketDefault.getBalance().getBalance(), HttpStatus.OK.toString(),
				HttpStatus.OK.name());


		transactionalLogRegister.log(loginRequest, response, 0, TransactionCod.LOGIN_TRX.getCode(), new BigDecimal(0));
		logRegister.logOut(loginRequest, response);
		
		return response;

	}
	
	
}
