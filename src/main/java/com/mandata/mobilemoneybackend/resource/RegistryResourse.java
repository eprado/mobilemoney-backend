package com.mandata.mobilemoneybackend.resource;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Master;
import com.mandata.mobilemoneybackend.exception.DuplicatedResourseException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.log.TransactionalLogRegister;
import com.mandata.mobilemoneybackend.model.TransactionCod;
import com.mandata.mobilemoneybackend.pocket.PocketComponent;
import com.mandata.mobilemoneybackend.request.RegistryRequest;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.MasterRepository;

@RestController
public class RegistryResourse {

	@Autowired
	private MasterRepository masterRepository;

	@Autowired
	private LogRegister logRegister;

	@Autowired
	private TransactionalLogRegister transactionalLogRegister;
	
	@Autowired
	private PocketComponent pocketComponent;
	

	@PostMapping("/registry")
	public ServiceResponse registerUser(@Valid @RequestBody RegistryRequest registryRequest) {

		//Registrar log de entrada
		logRegister.logIn(registryRequest);

		//Valida que no exista monedero para el tipo y número de identificación 
		Master master = masterRepository.findByIdentification(registryRequest.getBankCod(),
				registryRequest.getBin(), registryRequest.getCountryCod(), registryRequest.getIdType(), registryRequest.getIdNumber());

		if (master!=null) {
			logRegister.logOut(registryRequest, new ServiceResponse(HttpStatus.CONFLICT.toString(), HttpStatus.CONFLICT.name()));
			throw new DuplicatedResourseException("Account found with the same identification");
		}
		
		//Valida que el monedero no exista
		master =masterRepository.findByPrimaryKey(registryRequest.getBankCod(),registryRequest.getBin(), registryRequest.getCountryCod(),registryRequest.getPhoneNumber());
		
		if (master!=null) {
			logRegister.logOut(registryRequest, new ServiceResponse(HttpStatus.CONFLICT.toString(), HttpStatus.CONFLICT.name()));
			throw new DuplicatedResourseException("Account found with the same phone number");
		}
		
		ModelMapper modelMapper = new ModelMapper();
		Master masterSave = modelMapper.map(registryRequest, Master.class);

		masterRepository.save(masterSave);
		pocketComponent.createPocketDefault(registryRequest);
		
		ServiceResponse serviceResponse = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());

		transactionalLogRegister.log(registryRequest, serviceResponse, 0, TransactionCod.REGISTRY_TRX.getCode(), new BigDecimal(0));
		logRegister.logOut(registryRequest, serviceResponse);

		return serviceResponse;
	}
}
