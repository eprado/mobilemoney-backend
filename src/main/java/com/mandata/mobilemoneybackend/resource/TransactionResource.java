package com.mandata.mobilemoneybackend.resource;

import java.math.BigDecimal;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Pocket;
import com.mandata.mobilemoneybackend.exception.ResourceNotFoundException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.log.TransactionalLogRegister;
import com.mandata.mobilemoneybackend.model.TransactionCod;
import com.mandata.mobilemoneybackend.pocket.PocketComponent;
import com.mandata.mobilemoneybackend.request.PocketTransferRequest;
import com.mandata.mobilemoneybackend.request.TransferRequest;
import com.mandata.mobilemoneybackend.request.WithdrawalRequest;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.response.TransactionResponse;
import com.mandata.mobilemoneybackend.response.WithdrawalResponse;
import com.mandata.mobilemoneybackend.service.PocketRepository;

@RestController
public class TransactionResource {

	@Autowired
	private PocketComponent pocketComp;

	@Autowired
	private PocketRepository repository;

	@Autowired
	private LogRegister logRegister;

	@Autowired
	private TransactionalLogRegister transactionalLogRegister;

	
	
	@PostMapping("/withdrawal")
	public WithdrawalResponse withdrawal(@Valid @RequestBody WithdrawalRequest request) throws Exception {

		// Registra log de entrada
		logRegister.logIn(request);

		// Valida que tenga fondos para retiro
		Pocket pocketDefault = repository.findDefault(request.getBankCod(), request.getBin(), request.getPhoneNumber());
		
		if(pocketDefault==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("phoneNumber: " + request.getPhoneNumber());			
		}

		BigDecimal pocketDefaultBalance = pocketDefault.getBalance().getBalance()
				.add(pocketDefault.getBalance().getDebitAmount()).subtract(pocketDefault.getBalance().getCreditAmount())
				.subtract(pocketDefault.getBalance().getPawnedAmount());

		if (request.getWithdrawalAmount().compareTo(pocketDefaultBalance) == 1) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(),
					HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Insufficient Funds");
		}

		// Aplica Retiro
		BigDecimal currentCredit = pocketDefault.getBalance().getCreditAmount();
		pocketDefault.getBalance().setCreditAmount(currentCredit.add(request.getWithdrawalAmount()));

		repository.save(pocketDefault);

		//Consulta Saldo Total
		BigDecimal totalBalance = pocketComp.getTotalBalance(request.getBankCod(), request.getBin(),
				request.getPhoneNumber());

		//Genera OTP de Retiro
		String otp = String.valueOf(100000 + new Random().nextInt(899999));

		WithdrawalResponse response = new WithdrawalResponse(totalBalance,
				pocketDefaultBalance.subtract(request.getWithdrawalAmount()), pocketDefault.getPocketState(),
				HttpStatus.OK.toString(), HttpStatus.OK.name(), otp);

		// Registra Log transaccional
		transactionalLogRegister.log(request, response, pocketDefault.getPocketId(), TransactionCod.WITHDRAWAL_TRX.getCode(), request.getWithdrawalAmount());
		
		// Registra log de entrada
		logRegister.logOut(request, response);

		return response;
	}

	
	@PostMapping("/transfer")
	public TransactionResponse transfer(@Valid @RequestBody TransferRequest request) throws Exception {

		// Registra log de entrada
		logRegister.logIn(request);

		// Valida que no sea mismo número
		if (request.getPhoneNumberTargetAccount().equals(request.getPhoneNumber())) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Cannot transfer to the same phone number");
		}
		// Valida que tenga fondos
		Pocket pocketDefault = repository.findDefault(request.getBankCod(), request.getBin(), request.getPhoneNumber());
		
		if(pocketDefault==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("phoneNumber: " + request.getPhoneNumber());			
		}

		BigDecimal pocketDefaultBalance = pocketDefault.getBalance().getBalance()
				.add(pocketDefault.getBalance().getDebitAmount()).subtract(pocketDefault.getBalance().getCreditAmount())
				.subtract(pocketDefault.getBalance().getPawnedAmount());

		if (request.getTransferAmount().compareTo(pocketDefaultBalance) == 1) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Insufficient Funds");
		}

		// Consulta Cuenta Destino
		Pocket pocketTargetDefault = repository.findDefaultByBankAndPhone(request.getBankCod(),
				request.getPhoneNumberTargetAccount());

		if (pocketTargetDefault == null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Target Account not found");
		}
		
		// Aplica la transferencia
		pocketDefault.getBalance()
				.setCreditAmount(pocketDefault.getBalance().getCreditAmount().add(request.getTransferAmount()));
		pocketTargetDefault.getBalance()
				.setDebitAmount(pocketTargetDefault.getBalance().getDebitAmount().add(request.getTransferAmount()));

		repository.save(pocketDefault);
		repository.save(pocketTargetDefault);

		// Calcula Saldo Total
		BigDecimal totalBalance = pocketComp.getTotalBalance(request.getBankCod(), request.getBin(),
				request.getPhoneNumber());

		TransactionResponse response = new TransactionResponse(totalBalance,
				pocketDefaultBalance.subtract(request.getTransferAmount()), pocketDefault.getPocketState(), HttpStatus.OK.toString(), HttpStatus.OK.name());

		// Registra Log transaccional
		transactionalLogRegister.logTranfer(request, response, pocketDefault.getPocketId(), TransactionCod.TRANSFER_TRX.getCode(), request.getTransferAmount());

		// Registra Log de Salida
		logRegister.logOut(request, response);

		return response;
	}
	
	
	
	
	@PostMapping("/pocketTransfer")
	public ServiceResponse transfer(@Valid @RequestBody PocketTransferRequest request) throws Exception {
		
		// Registra log de entrada
		logRegister.logIn(request);
		
		Pocket pocketFrom = repository.findByPrimaryKey(request.getBankCod(), request.getBin(), request.getPocketId(), request.getPhoneNumber());
		
		if(pocketFrom==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("phoneNumber: " + request.getPhoneNumber());			
		}
		
		//Validar si tiene saldo en bolsillo Default
		BigDecimal pocketDefaultBalance = pocketFrom.getBalance().getBalance()
				.add(pocketFrom.getBalance().getDebitAmount()).subtract(pocketFrom.getBalance().getCreditAmount())
				.subtract(pocketFrom.getBalance().getPawnedAmount());

		if (request.getTransferAmount().compareTo(pocketDefaultBalance) == 1) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Insufficient Funds");
		}
		
		
		Pocket pocketTo = repository.findByPrimaryKey(request.getBankCod(), request.getBin(), request.getPocketTargetId(), request.getPhoneNumber());
		
		if(pocketTo==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("phoneNumber: " + request.getPhoneNumber());			
		}
		
		pocketFrom.getBalance().setCreditAmount(pocketFrom.getBalance().getCreditAmount().add(request.getTransferAmount()));
		pocketTo.getBalance().setDebitAmount(pocketTo.getBalance().getDebitAmount().add(request.getTransferAmount()));
		
		repository.save(pocketFrom);
		repository.save(pocketTo);
		
		ServiceResponse response = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());
		
		// Registra Log transaccional
		transactionalLogRegister.log(request, response, pocketFrom.getPocketId(), TransactionCod.TRANSFER_POCKET_TRX.getCode(), request.getTransferAmount());

		// Registra Log de Salida
		logRegister.logOut(request, response);
		
		return response;
	}

}
