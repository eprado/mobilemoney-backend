package com.mandata.mobilemoneybackend.resource;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Master;
import com.mandata.mobilemoneybackend.entity.OtpRegistry;
import com.mandata.mobilemoneybackend.exception.DuplicatedResourseException;
import com.mandata.mobilemoneybackend.exception.ResourceNotFoundException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.request.OtpRequest;
import com.mandata.mobilemoneybackend.response.OtpRegistryResponse;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.MasterRepository;
import com.mandata.mobilemoneybackend.service.OtpRegistryRepository;

@RestController
public class OtpRegistryResource {

	@Autowired
	private OtpRegistryRepository repository;

	@Autowired
	private MasterRepository masterRepository;
	
	@Autowired
	private LogRegister logRegister;

	private DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	
	@GetMapping("/otp/{countryCod}/{bankCod}/{bin}/{canal}/{phoneNumber}")
	public OtpRegistryResponse generateOtp(@Valid OtpRequest request, @PathVariable int countryCod, @PathVariable long bin) throws Exception {

		//Registrar log de entrada
		logRegister.logIn(request);

		//Validar que no exista aún en el maestro
		Master master = masterRepository.findByPrimaryKey(request.getBankCod(), bin, countryCod, request.getPhoneNumber());
		
		if(master!=null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.CONFLICT.toString(), HttpStatus.CONFLICT.name()));
			throw new DuplicatedResourseException("Account found with the same phone number");			
		}
		
		//Generar OTP
		String otp = String.valueOf(100000 + new Random().nextInt(899999));

		//Valida si existe para guardarlo o actualizarlo
		List<OtpRegistry> otpGenerated = repository.findByBankAndPhone(request.getBankCod(), request.getCanal(), request.getPhoneNumber());

		if (otpGenerated.isEmpty()) {
			OtpRegistry otpRegistry = new OtpRegistry(request.getBankCod(), request.getCanal(), request.getPhoneNumber(),
					dateFormat.format(new Date()).toString() + " " + hourFormat.format(new Date()).toString(), otp);
			repository.save(otpRegistry);
		} else
			repository.updateOtp(((OtpRegistry) otpGenerated.get(0)).getId(), otp);

		OtpRegistryResponse otpRegistryResponse = new OtpRegistryResponse(HttpStatus.OK.toString(),
				HttpStatus.OK.name(), otp);

		//Registrar log de salida
		logRegister.logOut(request, otpRegistryResponse);

		return otpRegistryResponse;
	}
	
	
	
	

	@GetMapping("/otp/validation/{bankCod}/{canal}/{phoneNumber}")
	public OtpRegistryResponse retrieveOtp(@Valid OtpRequest request) {

		//Registrar log de entrada
		logRegister.logIn(request);

		List<OtpRegistry> otpGenerated = repository.findByBankAndPhone(request.getBankCod(), request.getCanal(), request.getPhoneNumber());

		
		//Consulta OTP
		if (otpGenerated.isEmpty()) {
			logRegister.logOut(request,
					new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("phoneNumber:" + request.getPhoneNumber());

		} else {
			OtpRegistryResponse otpRegistryResponse = new OtpRegistryResponse(HttpStatus.OK.toString(),
					HttpStatus.OK.name(), otpGenerated.get(0).getOtp());
			logRegister.logOut(request, otpRegistryResponse);
			return otpRegistryResponse;
		}

	}

	
	
	@DeleteMapping("/otp/{bankCod}/{canal}/{phoneNumber}")
	public ServiceResponse deleteOtp(@Valid OtpRequest request) {

		//Registrar log de entrada
		logRegister.logIn(request);

		//Consulta OTP para eliminación
		List<OtpRegistry> otpGenerated = repository.findByBankAndPhone(request.getBankCod(), request.getCanal(), request.getPhoneNumber());
		if (!otpGenerated.isEmpty())
			repository.deleteOtp(request.getBankCod(), request.getCanal(), request.getPhoneNumber());
		else {
			logRegister.logOut(request,
					new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("bankCod:" + request.getBankCod() + " - phoneNumber:" + request.getPhoneNumber());
		}

		ServiceResponse response = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());

		//Registrar log de salida
		logRegister.logOut(request, response);
		
		return response;
	}

}
