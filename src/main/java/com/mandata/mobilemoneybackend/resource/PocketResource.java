package com.mandata.mobilemoneybackend.resource;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Balance;
import com.mandata.mobilemoneybackend.entity.Pocket;
import com.mandata.mobilemoneybackend.exception.ResourceNotFoundException;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.log.TransactionalLogRegister;
import com.mandata.mobilemoneybackend.model.PocketData;
import com.mandata.mobilemoneybackend.model.TransactionCod;
import com.mandata.mobilemoneybackend.pocket.PocketComponent;
import com.mandata.mobilemoneybackend.request.PocketCreationRequest;
import com.mandata.mobilemoneybackend.request.PocketDataRequest;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.PocketRepository;

@RestController
public class PocketResource {

	@Autowired
	private PocketComponent pocketComp;

	@Autowired
	private PocketRepository repository;

	@Autowired
	private LogRegister logRegister;

	@Autowired
	private TransactionalLogRegister transactionalLogRegister;
	
	

	@PostMapping("/pocket")
	public ServiceResponse create(@Valid @RequestBody PocketCreationRequest request) throws Exception {

		//Registrar Log de entrada
		logRegister.logIn(request);

		int pocketID = pocketComp.getNewPocketId(request.getBankCod(), request.getBin(), request.getPhoneNumber());

		Balance balance = new Balance(request.getPhoneNumber(), pocketID, request.getBankCod(), request.getBin(), "",
				new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), new BigDecimal(0), request.getPocketBalance(),
				0, 0);

		Pocket pocket = new Pocket(request.getBankCod(), request.getBin(), pocketID, request.getPhoneNumber(),
				request.getPocketName(), PocketData.NEW_DESC.getValue(), PocketData.NEW_STATE.getValue(),
				PocketData.NEW_IS_DEFAULT.getValue(), balance);

		repository.save(pocket);

		ServiceResponse serviceResponse = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());

		transactionalLogRegister.log(request, serviceResponse, pocketID, TransactionCod.POCKET_CREATION_TRX.getCode(),
				new BigDecimal(0));
		logRegister.logOut(request, serviceResponse);

		return serviceResponse;
	}

	
	
	@DeleteMapping("/pocket")
	public ServiceResponse deletePocket (@Valid @RequestBody PocketDataRequest request) throws Exception {
		
		//Registrar Log de entrada
		logRegister.logIn(request);
		
		//Buscar registro
		Pocket pocket = repository.findByPrimaryKey(request.getBankCod(), request.getBin(), request.getPocketId(), request.getPhoneNumber());
		
		if(pocket==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("Bank = "+ request.getBankCod() +" Bin= "+ request.getBin() +" PocketId= "+ request.getPocketId() +" PhoneNumber= " + request.getPhoneNumber());			
		}
		
		if(pocket.getIsDefault().equals(PocketData.DEFAULT_IS_DEFAULT.getValue())){
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Cannot remove Default pocket");			
		}
		
		
		BigDecimal pocketBalance = pocket.getBalance().getBalance()
				.add(pocket.getBalance().getDebitAmount()).subtract(pocket.getBalance().getCreditAmount())
				.subtract(pocket.getBalance().getPawnedAmount());
		
		if(pocketBalance.compareTo(new BigDecimal(0))!= 0){
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Cannot remove for pending amount");			
		}
			
		repository.delete(pocket);
		
		ServiceResponse respose = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());

		transactionalLogRegister.log(request, respose, request.getPocketId(), TransactionCod.DELETE_POCKET_TRX.getCode(), new BigDecimal(0));
	
		logRegister.logOut(request, respose);
		
		return respose;
	}
	
	
	
	
	@PutMapping("/pocketName")
	public ServiceResponse changeName (@Valid @RequestBody PocketDataRequest request) {
		
		//Registrar Log de entrada
		logRegister.logIn(request);
		
		//Buscar registro
		Pocket pocket = repository.findByPrimaryKey(request.getBankCod(), request.getBin(), request.getPocketId(), request.getPhoneNumber());
		
		if(pocket==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("Bank = "+ request.getBankCod() +" Bin= "+ request.getBin() +" PocketId= "+ request.getPocketId() +" PhoneNumber= " + request.getPhoneNumber());			
		}
		
		pocket.setPocketName(request.getPocketName()==null||request.getPocketName().equals("")?"-":request.getPocketName());
		
		repository.save(pocket);
		
		ServiceResponse respose = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());

		transactionalLogRegister.log(request, respose, request.getPocketId(), TransactionCod.CHANGE_NAME_TRX.getCode(), new BigDecimal(0));
	
		logRegister.logOut(request, respose);
		
		return respose;
		
	}
	
	
	
	@PutMapping("/pocket/assign")
	public ServiceResponse assign (@Valid @RequestBody PocketDataRequest request) throws Exception {
		
		//Registrar Log de entrada
		logRegister.logIn(request);
		
		//Buscar registro
		Pocket pocket = repository.findByPrimaryKey(request.getBankCod(), request.getBin(), request.getPocketId(), request.getPhoneNumber());
		
		if(pocket==null) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.NOT_FOUND.toString(), HttpStatus.NOT_FOUND.name()));
			throw new ResourceNotFoundException("Bank = "+ request.getBankCod() +" Bin= "+ request.getBin() +" PocketId= "+ request.getPocketId() +" PhoneNumber= " + request.getPhoneNumber());			
		}
		
		Pocket pocketDefault = repository.findDefault(request.getBankCod(), request.getBin(), request.getPhoneNumber());
		
		if(pocketDefault.getPocketId()==request.getPocketId()){
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception("Pocket is already Default");			
		}
		
		pocket.setIsDefault(PocketData.DEFAULT_IS_DEFAULT.getValue());
		pocketDefault.setIsDefault(PocketData.NEW_IS_DEFAULT.getValue());
		
		repository.save(pocket);
		repository.save(pocketDefault);
		
		ServiceResponse respose = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());

		transactionalLogRegister.log(request, respose, request.getPocketId(), TransactionCod.ASSIGN_POCKET_TRX.getCode(), new BigDecimal(0));
	
		logRegister.logOut(request, respose);
		
		return respose;
		
	}

}
