package com.mandata.mobilemoneybackend.resource;

import java.math.BigDecimal;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mandata.mobilemoneybackend.entity.Master;
import com.mandata.mobilemoneybackend.log.LogRegister;
import com.mandata.mobilemoneybackend.log.TransactionalLogRegister;
import com.mandata.mobilemoneybackend.model.TransactionCod;
import com.mandata.mobilemoneybackend.request.PasswordRequest;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.MasterRepository;

@RestController
public class PasswordResource {
	
	@Autowired
	private MasterRepository repository;

	@Autowired
	LogRegister logRegister;

	@Autowired
	TransactionalLogRegister transactionalLogRegister;
	
	
	@PostMapping("/password/change")
	public ServiceResponse changePassword (@Valid @RequestBody PasswordRequest request) throws Exception {
		
		//Registrar Log de entrada
		logRegister.logIn(request);
				
		//Valida Password Actual
		Master master = repository.findByPrimaryKey(request.getBankCod(), request.getBin(), request.getCountryCod(), request.getPhoneNumber());
		
		if(!master.getPassword().equals(request.getCurrentPassword())) {
			logRegister.logOut(request, new ServiceResponse(HttpStatus.INTERNAL_SERVER_ERROR.toString(), HttpStatus.INTERNAL_SERVER_ERROR.name()));
			throw new Exception ("Invalid Current Password");
		}
		
		master.setPassword(request.getNewPassword());
		
		//Actualiza Nuevo Password
		repository.save(master);

		ServiceResponse response = new ServiceResponse(HttpStatus.OK.toString(), HttpStatus.OK.name());
		
		// Registra Log transaccional
		transactionalLogRegister.log(request, response, 0, TransactionCod.CHANGE_PASSWORD_TRX.getCode(), new BigDecimal(0));
		
		// Registra log de entrada
		logRegister.logOut(request, response);

		return response;

	}

}
