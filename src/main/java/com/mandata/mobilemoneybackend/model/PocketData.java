package com.mandata.mobilemoneybackend.model;

public enum PocketData {

	DEFAULT_POCKET_ID("1"), 
	DEFAULT_SHORT_NAME("Default"), 
	DEFAULT_DESC("Default Pocket"), 
	DEFAULT_STATE("A"), 
	DEFAULT_IS_DEFAULT("S"), 
	DEFAULT_BALANCE("2000000"),	
	NEW_SHORT_NAME("New Pocket"), 
	NEW_DESC("New Pocket"), 
	NEW_STATE("A"), 
	NEW_IS_DEFAULT("N");

	private final String value;

	private PocketData(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
	

}
