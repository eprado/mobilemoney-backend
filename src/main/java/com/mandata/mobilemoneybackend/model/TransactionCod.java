package com.mandata.mobilemoneybackend.model;

public enum TransactionCod {
	
	REGISTRY_TRX(800000),
	WITHDRAWAL_TRX(11000),
	TRANSFER_TRX(841000),
	TRANSFER_POCKET_TRX(960000),
	BALANCE_TRX(311000),
	POCKET_CREATION_TRX(930000),
	MOVEMENTS_TRX(851000),
	CHANGE_PASSWORD_TRX(900000),
	CHANGE_NAME_TRX(960000),
	DELETE_POCKET_TRX(950000),
	ASSIGN_POCKET_TRX(940000),
	LOGIN_TRX(810000);
	
	private final int code;

	private TransactionCod(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
	
	

}
