package com.mandata.mobilemoneybackend.pocket;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mandata.mobilemoneybackend.entity.Balance;
import com.mandata.mobilemoneybackend.entity.Pocket;
import com.mandata.mobilemoneybackend.model.PocketData;
import com.mandata.mobilemoneybackend.request.RegistryRequest;
import com.mandata.mobilemoneybackend.service.BalanceRepository;
import com.mandata.mobilemoneybackend.service.PocketRepository;

@Component
public class PocketComponent {

	@Autowired
	PocketRepository pocketRepository;

	@Autowired
	BalanceRepository balanceRepository;
	

	public void createPocketDefault(RegistryRequest registryRequest) {

		Balance balance = new Balance(registryRequest.getPhoneNumber(), 1, registryRequest.getBankCod(),
				registryRequest.getBin(), "", new BigDecimal(0), new BigDecimal(0), new BigDecimal(0),
				new BigDecimal(0), new BigDecimal(PocketData.DEFAULT_BALANCE.getValue()), 0, 0);

		pocketRepository.save(
				new Pocket(registryRequest.getBankCod(), registryRequest.getBin(), 1, registryRequest.getPhoneNumber(),
						PocketData.DEFAULT_SHORT_NAME.getValue(), PocketData.DEFAULT_DESC.getValue(),
						PocketData.DEFAULT_STATE.getValue(), PocketData.DEFAULT_IS_DEFAULT.getValue(), balance));

	}
	
	
	public int getNewPocketId(int bankCod, long bin, String phoneNumber) throws Exception {

		List<Pocket> pocketList = pocketRepository.findPocketList(bankCod, bin, phoneNumber);
		
		if(pocketList.isEmpty())
			throw new Exception("Must exist a Default Pocket");
		
		Pocket pocket = pocketList.get(pocketList.size()-1);
		
		return pocket.getPocketId()+1;

	}
	
	
	public BigDecimal getTotalBalance (int bankCod, long bin, String phoneNumber) {
		
		List<Pocket> pocketList = pocketRepository.findPocketList(bankCod, bin, phoneNumber);
		
		BigDecimal totalBalance = new BigDecimal(0);

		for (Pocket pocket : pocketList) {

			BigDecimal pocketBalance = pocket.getBalance().getBalance();
			BigDecimal pocketDebit = pocket.getBalance().getDebitAmount();
			BigDecimal pocketCredit = pocket.getBalance().getCreditAmount();
			BigDecimal pocketPawned = pocket.getBalance().getPawnedAmount();

			pocketBalance = pocketBalance.add(pocketDebit).subtract(pocketCredit).subtract(pocketPawned);
			totalBalance = totalBalance.add(pocketBalance);
		}
		
		return totalBalance;
	}

}
