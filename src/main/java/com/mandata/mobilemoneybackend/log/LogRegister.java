package com.mandata.mobilemoneybackend.log;

import java.text.SimpleDateFormat;
//import java.sql.Date;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mandata.mobilemoneybackend.entity.Log;
import com.mandata.mobilemoneybackend.request.ServiceRequest;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.LogRepository;

@Component
public class LogRegister {
	
	@Autowired
	private LogRepository repository;
	
	private SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
	private SimpleDateFormat sdfHour = new SimpleDateFormat("HHmmss");
	
	
	public void logIn (ServiceRequest serviceRequest) {
		
	    Date now = new Date();
		Log log = new Log(serviceRequest.getBankCod(), serviceRequest.getCanal(), "I", sdfDate.format(now), sdfHour.format(now), "",
				"", "", serviceRequest.toString());
			
		repository.save(log);
		
	}
	

	public void logOut (ServiceRequest serviceRequest, ServiceResponse serviceResponse) {
		
	    Date now = new Date();
		Log log = new Log(serviceRequest.getBankCod(), serviceRequest.getCanal(), "O", sdfDate.format(now), sdfHour.format(now), serviceResponse.getReponseCod(),
				serviceResponse.getResponseDesc(), "", serviceResponse.toString());
			
		repository.save(log);
		
	}
	
	
}
