package com.mandata.mobilemoneybackend.log;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mandata.mobilemoneybackend.entity.TransactionalLog;
import com.mandata.mobilemoneybackend.request.LoginRequest;
import com.mandata.mobilemoneybackend.request.TransactionRequest;
import com.mandata.mobilemoneybackend.request.TransferRequest;
import com.mandata.mobilemoneybackend.response.ServiceResponse;
import com.mandata.mobilemoneybackend.service.TransactionalLogRepository;

@Component
public class TransactionalLogRegister {

	@Autowired
	private TransactionalLogRepository repository;

	private SimpleDateFormat sdfDate = new SimpleDateFormat("yyyyMMdd");
	private SimpleDateFormat sdfHour = new SimpleDateFormat("HHmmss");

	public void log(TransactionRequest transactionRequest, ServiceResponse serviceResponse, int pocketId,
			int transactionCod, BigDecimal amount) {

		Date now = new Date();
		repository.save(new TransactionalLog(transactionRequest.getBankCod(), transactionRequest.getBin(),
				transactionRequest.getCountryCod(), transactionRequest.getPhoneNumber(), pocketId,
				transactionRequest.getCanal(), transactionCod, sdfDate.format(now), sdfHour.format(now), "", amount,
				serviceResponse.getReponseCod(), serviceResponse.getResponseDesc(), 0, 0, "",
				serviceResponse.getAuthNumber()));

	}

	public void log(LoginRequest serviceRequest, ServiceResponse serviceResponse, int pocketId, int transactionCod,
			BigDecimal amount) {

		Date now = new Date();
		repository.save(new TransactionalLog(serviceRequest.getBankCod(), serviceRequest.getBin(),
				serviceRequest.getCountryCod(), "", pocketId, serviceRequest.getCanal(), transactionCod,
				sdfDate.format(now), sdfHour.format(now), "", amount, serviceResponse.getReponseCod(),
				serviceResponse.getResponseDesc(), 0, 0, "", serviceResponse.getAuthNumber()));

	}

	public void logTranfer(TransferRequest transactionRequest, ServiceResponse serviceResponse, int pocketId,
			int transactionCod, BigDecimal amount) {

		Date now = new Date();
		repository.save(new TransactionalLog(transactionRequest.getBankCod(), transactionRequest.getBin(),
				transactionRequest.getCountryCod(), transactionRequest.getPhoneNumber(), pocketId,
				transactionRequest.getCanal(), transactionCod, sdfDate.format(now), sdfHour.format(now), "", amount,
				serviceResponse.getReponseCod(), serviceResponse.getResponseDesc(),
				transactionRequest.getCountryCodTargetAccount(), transactionRequest.getBankCod(),
				transactionRequest.getPhoneNumberTargetAccount(), serviceResponse.getAuthNumber()));

	}

}
