package com.mandata.mobilemoneybackend.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="BMLM")
public class TransactionalLog {
	
	@JsonIgnore
	@Id
	@Column(name="BLMIDE")	
	private long id;
	@JsonIgnore
	@Column(name="BLMBCO")	
	private int bankCod;
	@JsonIgnore
	@Column(name="BLMBIN")
	private long bin;
	@JsonIgnore
	@Column(name="BLMPAI")
	private int countryCod;
	@JsonIgnore
	@Column(name="BLMTEL")
	private String phoneNumber;
	@JsonIgnore
	@Column(name="BLMBOL")
	private int pocketId;
	@JsonIgnore
	@Column(name="BLMCNL")
	private String canal;
	@Column(name="BLMTRA")
	private int transactionCod;
	@Column(name="BLMTFE")
	private String transactionDate;
	@Column(name="BLMTHO")
	private String transactionHour;
	@JsonIgnore
	@Column(name="BLMTUS")
	private String user;
	@Column(name="BLMMTO")
	private BigDecimal amount;
	@Column(name="BLMRTA")
	private String responseCod;
	@JsonIgnore
	@Column(name="BLMDES")
	private String responseDesc;
	@Column(name="BLMPAD")
	private int targetCountry;
	@JsonIgnore
	@Column(name="BLMBCD")
	private int targetBank;
	@Column(name="BLMTED")
	private String targetPhoneNumber;
	@Column(name="BLMAUT")
	private String authNumber;
	
	
	public TransactionalLog() {
		super();
	}


	public TransactionalLog(int bankCod, long bin, int countryCod, String phoneNumber, int pocketId, String canal,
			int transactionCod, String transactionDate, String transactionHour, String user, BigDecimal amount,
			String responseCod, String responseDesc, int targetCountry, int targetBank, String targetPhoneNumber,
			String authNumber) {
		super();
		this.bankCod = bankCod;
		this.bin = bin;
		this.countryCod = countryCod;
		this.phoneNumber = phoneNumber;
		this.pocketId = pocketId;
		this.canal = canal;
		this.transactionCod = transactionCod;
		this.transactionDate = transactionDate;
		this.transactionHour = transactionHour;
		this.user = user;
		this.amount = amount;
		this.responseCod = responseCod;
		this.responseDesc = responseDesc;
		this.targetCountry = targetCountry;
		this.targetBank = targetBank;
		this.targetPhoneNumber = targetPhoneNumber;
		this.authNumber = authNumber;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public int getBankCod() {
		return bankCod;
	}


	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}


	public long getBin() {
		return bin;
	}


	public void setBin(long bin) {
		this.bin = bin;
	}


	public int getCountryCod() {
		return countryCod;
	}


	public void setCountryCod(int countryCod) {
		this.countryCod = countryCod;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public int getPocketId() {
		return pocketId;
	}


	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}


	public String getCanal() {
		return canal;
	}


	public void setCanal(String canal) {
		this.canal = canal;
	}


	public int getTransactionCod() {
		return transactionCod;
	}


	public void setTransactionCod(int transactionCod) {
		this.transactionCod = transactionCod;
	}


	public String getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}


	public String getTransactionHour() {
		return transactionHour;
	}


	public void setTransactionHour(String transactionHour) {
		this.transactionHour = transactionHour;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public BigDecimal getAmount() {
		return amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getResponseCod() {
		return responseCod;
	}


	public void setResponseCod(String responseCod) {
		this.responseCod = responseCod;
	}


	public String getResponseDesc() {
		return responseDesc;
	}


	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}


	public int getTargetCountry() {
		return targetCountry;
	}


	public void setTargetCountry(int targetCountry) {
		this.targetCountry = targetCountry;
	}


	public int getTargetBank() {
		return targetBank;
	}


	public void setTargetBank(int targetBank) {
		this.targetBank = targetBank;
	}


	public String getTargetPhoneNumber() {
		return targetPhoneNumber;
	}


	public void setTargetPhoneNumber(String targetPhoneNumber) {
		this.targetPhoneNumber = targetPhoneNumber;
	}


	public String getAuthNumber() {
		return authNumber;
	}


	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}
	
	
	
}
