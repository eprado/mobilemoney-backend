package com.mandata.mobilemoneybackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;


@Entity
@Table(name="BMMA")
@IdClass(MasterId.class)
public class Master {
	
	@Id
	@Column(name="BMABCO")
	private int bankCod;
	
	@Id
	@Column(name="BMATEL")
	@NotBlank
	private String phoneNumber;

	@Column(name="BMABIN")
	private long bin;
	
	@Column(name="BMAPAI")
	private int countryCod;
	
	@Column(name="BMAEST")
	private String actualState;
	
	@Column(name="BMATID")
	private String idType;
	
	@Column(name="BMANID")
	private String idNumber;
	
	@Column(name="BMANM1")
	private String firstName;
	
	@Column(name="BMANM2")
	private String secondName;
	
	@Column(name="BMAAP1")
	private String firstLastName;
	
	@Column(name="BMAAP2")
	private String secondLastName;
	
	@Column(name="BMACOR") 
	private String email;
	
	@Column(name="BMAIME")
	private String imei;
	
	@Column(name="BMAFEX")
	private String idExpeditionDate;
	
	@Column(name="BMAARM")
	private String accept1;
	
	@Column(name="BMAAPR")
	private String accept2;
	
	@Column(name="BMAACD")
	private String accept3;
	
	@Column(name="BMAOFR") 
	private String password;
	

	
	public Master() {
		super();
	}


	public Master(int bankCod, String phoneNumber, long bin, int countryCod, String actualState, String idType,
			String idNumber, String firstName, String secondName, String firstLastName, String secondLastName,
			String email, String imei, String idExpeditionDate, String accept1, String accept2, String accept3, String password) {
		super();
		this.bankCod = bankCod;
		this.phoneNumber = phoneNumber;
		this.bin = bin;
		this.countryCod = countryCod;
		this.actualState = actualState;
		this.idType = idType;
		this.idNumber = idNumber;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstLastName = firstLastName;
		this.secondLastName = secondLastName;
		this.email = email;
		this.imei = imei;
		this.idExpeditionDate = idExpeditionDate;
		this.accept1 = accept1;
		this.accept2 = accept2;
		this.accept3 = accept3;
		this.password = password;
	}





	public int getBankCod() {
		return bankCod;
	}





	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}





	public String getPhoneNumber() {
		return phoneNumber;
	}





	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}





	public long getBin() {
		return bin;
	}





	public void setBin(long bin) {
		this.bin = bin;
	}





	public int getCountryCod() {
		return countryCod;
	}





	public void setCountryCod(int countryCod) {
		this.countryCod = countryCod;
	}





	public String getActualState() {
		return actualState;
	}





	public void setActualState(String actualState) {
		this.actualState = actualState;
	}





	public String getIdType() {
		return idType;
	}





	public void setIdType(String idType) {
		this.idType = idType;
	}





	public String getIdNumber() {
		return idNumber;
	}





	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}





	public String getFirstName() {
		return firstName;
	}





	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}





	public String getSecondName() {
		return secondName;
	}





	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}





	public String getFirstLastName() {
		return firstLastName;
	}





	public void setFirstLastName(String firstLastName) {
		this.firstLastName = firstLastName;
	}





	public String getSecondLastName() {
		return secondLastName;
	}





	public void setSecondLastName(String secondLastName) {
		this.secondLastName = secondLastName;
	}





	public String getEmail() {
		return email;
	}





	public void setEmail(String email) {
		this.email = email;
	}





	public String getImei() {
		return imei;
	}





	public void setImei(String imei) {
		this.imei = imei;
	}





	public String getIdExpeditionDate() {
		return idExpeditionDate;
	}





	public void setIdExpeditionDate(String idExpeditionDate) {
		this.idExpeditionDate = idExpeditionDate;
	}





	public String getAccept1() {
		return accept1;
	}





	public void setAccept1(String accept1) {
		this.accept1 = accept1;
	}





	public String getAccept2() {
		return accept2;
	}





	public void setAccept2(String accept2) {
		this.accept2 = accept2;
	}





	public String getAccept3() {
		return accept3;
	}





	public void setAccept3(String accept3) {
		this.accept3 = accept3;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	
	
	

}
