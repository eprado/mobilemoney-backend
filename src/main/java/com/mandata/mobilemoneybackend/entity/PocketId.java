package com.mandata.mobilemoneybackend.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PocketId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private int bankCod;
	
	private long bin;
	
	private int pocketId;
	
	private String phoneNumber;

	
	public PocketId(int bankCod, long bin, int pocketId, String phoneNumber) {
		super();
		this.bankCod = bankCod;
		this.bin = bin;
		this.pocketId = pocketId;
		this.phoneNumber = phoneNumber;
	}

	public PocketId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public int getPocketId() {
		return pocketId;
	}

	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public long getBin() {
		return bin;
	}

	public void setBin(long bin) {
		this.bin = bin;
	}
	
	
	

}
