package com.mandata.mobilemoneybackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="BMLG")
public class Log {
	

	@Id
	@Column(name="BLGIDE") 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(name="BLGBCO") 
	private int bankCod;
	
	@Column(name="BLGCNL") 
	private String canal;
	
	@Column(name="BLGTIP") 
	private String logType;
	
	@Column(name="BLGFEC") 
	private String dateLog;
	
	@Column(name="BLGHOR") 
	private String hourLog;
	
	@Column(name="BLGRTA") 
	private String reponseCod;
	
	@Column(name="BLGDRT") 
	private String responseDesc;
	
	@Column(name="BLGUSR") 
	private String user;
	
	@Column(name="BLGMSG") 
	private String message;
	
	
	

	public Log() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Log(int bankCod, String canal, String logType, String dateLog, String hourLog, String reponseCod,
			String responseDesc, String user, String message) {
		super();
		this.bankCod = bankCod;
		this.canal = canal;
		this.logType = logType;
		this.dateLog = dateLog;
		this.hourLog = hourLog;
		this.reponseCod = reponseCod;
		this.responseDesc = responseDesc;
		this.user = user;
		this.message = message;
	}







	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getLogType() {
		return logType;
	}

	public void setLogType(String logType) {
		this.logType = logType;
	}
	
	public String getDateLog() {
		return dateLog;
	}

	public void setDateLog(String dateLog) {
		this.dateLog = dateLog;
	}

	public String getHourLog() {
		return hourLog;
	}


	public void setHourLog(String hourLog) {
		this.hourLog = hourLog;
	}


	public String getReponseCod() {
		return reponseCod;
	}

	public void setReponseCod(String reponseCod) {
		this.reponseCod = reponseCod;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
