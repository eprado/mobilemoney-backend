package com.mandata.mobilemoneybackend.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="BMSM")
public class Balance implements Serializable {
	
	private static final long serialVersionUID = 8737356851033433501L;

	@Id
	@GeneratedValue
	@Column(name="BSMIDE")
	private	long id;
	
	@Column(name="BSMTEL")
	private	String phoneNumber;

	@Column(name="BSMBOL")
	private	int pocketId;

	@Column(name="BSMBCO")
	private	int bankCod;
	
	@Column(name="BSMBIN")
	private	long bin;
	
	@Column(name="BSMOFR")
	private	String offsetWithdraw;
	
	@Column(name="BSMVAD")
	private	BigDecimal debitAmount;

	@Column(name="BSMVAC")
	private	BigDecimal creditAmount;

	@Column(name="BSMVAP")
	private	BigDecimal pawnedAmount;

	@Column(name="BSMNAC")
	private	BigDecimal accumAmount;

	@Column(name="BSMSTO")
	private	BigDecimal balance;

	@Column(name="BSMCCI")
	private	long consClosure;

	@Column(name="BSMCRE")
	private	long consRefresh;
	
	public Balance() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Balance(String phoneNumber, int pocketId, int bankCod, long bin, String offsetWithdraw,
			BigDecimal debitAmount, BigDecimal creditAmount, BigDecimal pawnedAmount, BigDecimal accumAmount,
			BigDecimal balance, long consClosure, long consRefresh) {
		super();
		this.phoneNumber = phoneNumber;
		this.pocketId = pocketId;
		this.bankCod = bankCod;
		this.bin = bin;
		this.offsetWithdraw = offsetWithdraw;
		this.debitAmount = debitAmount;
		this.creditAmount = creditAmount;
		this.pawnedAmount = pawnedAmount;
		this.accumAmount = accumAmount;
		this.balance = balance;
		this.consClosure = consClosure;
		this.consRefresh = consRefresh;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getPocketId() {
		return pocketId;
	}

	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}

	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public long getBin() {
		return bin;
	}

	public void setBin(long bin) {
		this.bin = bin;
	}

	public String getOffsetWithdraw() {
		return offsetWithdraw;
	}

	public void setOffsetWithdraw(String offsetWithdraw) {
		this.offsetWithdraw = offsetWithdraw;
	}

	public BigDecimal getDebitAmount() {
		return debitAmount;
	}

	public void setDebitAmount(BigDecimal debitAmount) {
		this.debitAmount = debitAmount;
	}

	public BigDecimal getCreditAmount() {
		return creditAmount;
	}

	public void setCreditAmount(BigDecimal creditAmount) {
		this.creditAmount = creditAmount;
	}

	public BigDecimal getPawnedAmount() {
		return pawnedAmount;
	}

	public void setPawnedAmount(BigDecimal pawnedAmount) {
		this.pawnedAmount = pawnedAmount;
	}

	public BigDecimal getAccumAmount() {
		return accumAmount;
	}

	public void setAccumAmount(BigDecimal accumAmount) {
		this.accumAmount = accumAmount;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public long getConsClosure() {
		return consClosure;
	}

	public void setConsClosure(long consClosure) {
		this.consClosure = consClosure;
	}

	public long getConsRefresh() {
		return consRefresh;
	}

	public void setConsRefresh(long consRefresh) {
		this.consRefresh = consRefresh;
	}
	
	
}
