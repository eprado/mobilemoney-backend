package com.mandata.mobilemoneybackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "BMTI")
public class IdentificationType{
	
	@Id	
	@Column(name="BTITID") 
	private String idType;	
	
	@Column(name="BTIDES") 
	private String descriptionType;
	
	@Column(name="BTIEST") 
	private String state;
	
	@JsonIgnore
	@ManyToOne
    @JoinColumns({
		@JoinColumn(name="BTIBCO", referencedColumnName="BPGBCO"),
		@JoinColumn(name="BTICNL", referencedColumnName="BPGCNL")
	})
	private Parameters parameters;

			
	public IdentificationType() {
		super();
		// TODO Auto-generated constructor stub
	}


	
	public IdentificationType(String idType, String descriptionType, String state, Parameters parameters) {
		super();
		this.idType = idType;
		this.descriptionType = descriptionType;
		this.state = state;
		this.parameters = parameters;
	}


	


	public String getIdType() {
		return idType;
	}


	public void setIdType(String idType) {
		this.idType = idType;
	}


	public String getDescriptionType() {
		return descriptionType;
	}


	public void setDescriptionType(String descriptionType) {
		this.descriptionType = descriptionType;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public Parameters getParameters() {
		return parameters;
	}


	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}



}
