package com.mandata.mobilemoneybackend.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@JsonIdentityInfo(scope = Parameters.class,generator = ObjectIdGenerators.PropertyGenerator.class,property = "bankCod")
@Entity
@Table(name="BMPG")
@IdClass(ParametersId.class)
public class Parameters implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BPGBCO") 
	private int bankCod;
	
	@Id
	@Column(name="BPGCNL") 	
	private String canal;
	
	@Column(name="BPGBIN") 	
	private int bin;
	
	@Column(name="BPGPAI") 	
	private int countryCod;
	
	@Column(name="BPGVAP") 	
	private String applicationVersion;
	
	@Column(name="BPGTIM") 	
	private int sessionTimeOut;
	

	@OneToMany(mappedBy="parameters")
	private List<IdentificationType> identificationTypes = new ArrayList<>();
	
	
	
	public Parameters() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Parameters(int bankCod, String canal, int bin, int countryCod, String applicationVersion, int sessionTimeOut,
			List<IdentificationType> identificationTypes) {
		super();
		this.bankCod = bankCod;
		this.canal = canal;
		this.bin = bin;
		this.countryCod = countryCod;
		this.applicationVersion = applicationVersion;
		this.sessionTimeOut = sessionTimeOut;
		this.identificationTypes = identificationTypes;
	}



	public int getBankCod() {
		return bankCod;
	}



	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}



	public String getCanal() {
		return canal;
	}



	public void setCanal(String canal) {
		this.canal = canal;
	}



	public int getBin() {
		return bin;
	}



	public void setBin(int bin) {
		this.bin = bin;
	}



	public int getCountryCod() {
		return countryCod;
	}



	public void setCountryCod(int countryCod) {
		this.countryCod = countryCod;
	}



	public String getApplicationVersion() {
		return applicationVersion;
	}



	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}



	public int getSessionTimeOut() {
		return sessionTimeOut;
	}



	public void setSessionTimeOut(int sessionTimeOut) {
		this.sessionTimeOut = sessionTimeOut;
	}



	public List<IdentificationType> getIdentificationTypes() {
		return identificationTypes;
	}



	public void setIdentificationTypes(List<IdentificationType> identificationTypes) {
		this.identificationTypes = identificationTypes;
	}
	
	
		
}
