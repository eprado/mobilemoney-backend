package com.mandata.mobilemoneybackend.entity;

import java.io.Serializable;

public class BalanceId implements Serializable{
	
	private static final long serialVersionUID = 6939855964080934212L;
	

	private	String phoneNumber;

	private	int pocketId;
	
	private	int bankCod;
	
	private	long bin;
	
	public BalanceId() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BalanceId(String phoneNumber, int pocketId, int bankCod, long bin) {
		super();
		this.phoneNumber = phoneNumber;
		this.pocketId = pocketId;
		this.bankCod = bankCod;
		this.bin = bin;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public int getPocketId() {
		return pocketId;
	}

	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}

	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public long getBin() {
		return bin;
	}

	public void setBin(long bin) {
		this.bin = bin;
	}

	


	



}
