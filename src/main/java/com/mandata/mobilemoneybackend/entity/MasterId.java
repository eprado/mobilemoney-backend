package com.mandata.mobilemoneybackend.entity;

import java.io.Serializable;

public class MasterId implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private int bankCod;
	
	private String phoneNumber;

	
	
	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	
	

}
