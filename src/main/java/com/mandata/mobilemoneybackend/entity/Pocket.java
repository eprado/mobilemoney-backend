package com.mandata.mobilemoneybackend.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="BMBO")
@IdClass(PocketId.class)
public class Pocket {
	
	@Id
	@Column(name="BBOBCO")
	private int bankCod;
	
	@Id
	@Column(name="BBOBIN") 
	private long bin;
	
	@Id
	@Column(name="BBOBOL")
	private int pocketId;
	
	@Id
	@Column(name="BBOTEL")
	private String phoneNumber;
	
	@Column(name="BBONCO")
	private String pocketName;
	
	@Column(name="BBODES")
	private String pocketDesc;
	
	@Column(name="BBOEST")
	private String pocketState;
	
	@Column(name="BBODEF")	
	private String isDefault;
	
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="BBOSID")
	private Balance balance;
	
	
	public Pocket() {
		super();
	}


	public Pocket(int bankCod, long bin, int pocketId, String phoneNumber, String pocketName, String pocketDesc,
			String pocketState, String isDefault, Balance balance) {
		super();
		this.bankCod = bankCod;
		this.bin = bin;
		this.pocketId = pocketId;
		this.phoneNumber = phoneNumber;
		this.pocketName = pocketName;
		this.pocketDesc = pocketDesc;
		this.pocketState = pocketState;
		this.isDefault = isDefault;
		this.balance = balance;
	}


	public int getBankCod() {
		return bankCod;
	}


	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}


	public long getBin() {
		return bin;
	}


	public void setBin(long bin) {
		this.bin = bin;
	}


	public int getPocketId() {
		return pocketId;
	}


	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getPocketName() {
		return pocketName;
	}


	public void setPocketName(String pocketName) {
		this.pocketName = pocketName;
	}


	public String getPocketDesc() {
		return pocketDesc;
	}


	public void setPocketDesc(String pocketDesc) {
		this.pocketDesc = pocketDesc;
	}


	public String getPocketState() {
		return pocketState;
	}


	public void setPocketState(String pocketState) {
		this.pocketState = pocketState;
	}


	public String getIsDefault() {
		return isDefault;
	}


	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}


	public Balance getBalance() {
		return balance;
	}


	public void setBalance(Balance balance) {
		this.balance = balance;
	}


	

	

}
