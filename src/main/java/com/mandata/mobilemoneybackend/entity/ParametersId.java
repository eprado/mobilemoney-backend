package com.mandata.mobilemoneybackend.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;

public class ParametersId implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="BPGBCO") 
	private int bankCod;
	
	@Id
	@Column(name="BPGCNL") 	
	private String canal;
	
	

	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}
	
	
	

}
