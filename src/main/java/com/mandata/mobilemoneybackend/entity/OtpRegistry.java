package com.mandata.mobilemoneybackend.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="BMOT")
public class OtpRegistry {

	
	@Id
	@Column(name="BOTIDE")
	private int id;
	
	@Column(name="BOTBCO")
	private int bankCod;
	
	@Column(name="BOTCNL")
	private String canal;
		
	@Column(name="BOTTEL")
	private String phoneNumber;
	
	@Column(name="BOTFEC")
	private String date;
	
	@Column(name="BOTOTP")
	private String otp;
	
	

	public OtpRegistry() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public OtpRegistry(int bankCod, String canal, String phoneNumber, String date, String otp) {
		super();
		this.bankCod = bankCod;
		this.canal = canal;
		this.phoneNumber = phoneNumber;
		this.date = date;
		this.otp = otp;
	}

	
	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public int getBankCod() {
		return bankCod;
	}

	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}


	
	
	
	
}
