package com.mandata.mobilemoneybackend.response;

import java.math.BigDecimal;

public class WithdrawalResponse extends TransactionResponse{

	private String withdrawalOTP;
	
	public WithdrawalResponse() {
		super();
	}

	public WithdrawalResponse(BigDecimal totalBalance, BigDecimal pocketBalance, String pocketState, String reponseCod,
			String responseDesc, String withdrawalOTP) {
		super(totalBalance, pocketBalance, pocketState, reponseCod, responseDesc);
		this.withdrawalOTP = withdrawalOTP;
	}

	public String getWithdrawalOTP() {
		return withdrawalOTP;
	}

	public void setWithdrawalOTP(String withdrawalOTP) {
		this.withdrawalOTP = withdrawalOTP;
	}

	@Override
	public String toString() {
		return super.toString() + "-->WithdrawalResponse [withdrawalOTP=" + withdrawalOTP + "]";
	}
	

}
