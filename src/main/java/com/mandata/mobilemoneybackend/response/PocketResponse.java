package com.mandata.mobilemoneybackend.response;

import java.math.BigDecimal;

public class PocketResponse {
	
	private int pocketId;
	
	private String pocketName;
	
	private String pocketState;
	
	private BigDecimal pocketBalance;
	
	public PocketResponse() {
		super();
	}

	public PocketResponse(int pocketId, String pocketName, String pocketState, BigDecimal pocketBalance) {
		super();
		this.pocketId = pocketId;
		this.pocketName = pocketName;
		this.pocketState = pocketState;
		this.pocketBalance = pocketBalance;
	}

	public int getPocketId() {
		return pocketId;
	}

	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}

	public String getPocketName() {
		return pocketName;
	}

	public void setPocketName(String pocketName) {
		this.pocketName = pocketName;
	}

	public String getPocketState() {
		return pocketState;
	}

	public void setPocketState(String pocketState) {
		this.pocketState = pocketState;
	}

	public BigDecimal getPocketBalance() {
		return pocketBalance;
	}

	public void setPocketBalance(BigDecimal pocketBalance) {
		this.pocketBalance = pocketBalance;
	}

	@Override
	public String toString() {
		return "-->PocketResponse [pocketId=" + pocketId + ", pocketName=" + pocketName + ", pocketState=" + pocketState
				+ ", pocketBalance=" + pocketBalance + "]";
	}
	
	

}
