package com.mandata.mobilemoneybackend.response;

import java.util.Random;

public class ServiceResponse {
	
	private String reponseCod;
	
	private String responseDesc;
	
	private String authNumber;

	public ServiceResponse() {
		super();
		this.authNumber = String.valueOf(100000 + new Random().nextInt(899999));
	}

	public ServiceResponse(String reponseCod, String responseDesc) {
		super();
		this.reponseCod = reponseCod;
		this.responseDesc = responseDesc;
		this.authNumber = String.valueOf(100000 + new Random().nextInt(899999));
	}

	public String getReponseCod() {
		return reponseCod;
	}

	public void setReponseCod(String reponseCod) {
		this.reponseCod = reponseCod;
	}

	public String getResponseDesc() {
		return responseDesc;
	}

	public void setResponseDesc(String responseDesc) {
		this.responseDesc = responseDesc;
	}

	public String getAuthNumber() {
		return authNumber;
	}

	public void setAuthNumber(String authNumber) {
		this.authNumber = authNumber;
	}

	@Override
	public String toString() {
		return  "ServiceResponse [reponseCod=" + reponseCod + ", responseDesc=" + responseDesc + ", authNumber="
				+ authNumber + "]";
	}
	
	
	
}
