package com.mandata.mobilemoneybackend.response;

import java.math.BigDecimal;

public class LoginResponse extends ServiceResponse {
	
	
	private int pocketId;
	private String pocketName;
	private String pocketState;
	private BigDecimal pocketBalance;
	
	
	
	
	public LoginResponse() {
		super();
	}


	public LoginResponse(int pocketId, String pocketName, String pocketState, BigDecimal pocketBalance, String reponseCod, String responseDesc) {
		super(reponseCod, responseDesc);
		this.pocketId = pocketId;
		this.pocketName = pocketName;
		this.pocketState = pocketState;
		this.pocketBalance = pocketBalance;
	}




	public int getPocketId() {
		return pocketId;
	}


	public void setPocketId(int pocketId) {
		this.pocketId = pocketId;
	}


	public String getPocketName() {
		return pocketName;
	}


	public void setPocketName(String pocketName) {
		this.pocketName = pocketName;
	}


	public String getPocketState() {
		return pocketState;
	}


	public void setPocketState(String pocketState) {
		this.pocketState = pocketState;
	}


	public BigDecimal getPocketBalance() {
		return pocketBalance;
	}


	public void setPocketBalance(BigDecimal pocketBalance) {
		this.pocketBalance = pocketBalance;
	}


	@Override
	public String toString() {
		return super.toString() + "-->LoginResponse [pocketId=" + pocketId + ", pocketName=" + pocketName + ", pocketState=" + pocketState
				+ ", pocketBalance=" + pocketBalance + "]";
	}
	
	
	
	


}
