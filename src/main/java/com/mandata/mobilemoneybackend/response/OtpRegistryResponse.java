package com.mandata.mobilemoneybackend.response;

public class OtpRegistryResponse extends ServiceResponse {
	
	private String otp;

	
	public OtpRegistryResponse(String reponseCod, String responseDesc, String otp) {
		super(reponseCod, responseDesc);
		this.otp = otp;
	}


	public OtpRegistryResponse() {
		super();
		// TODO Auto-generated constructor stub
	}


	public String getOtp() {
		return otp;
	}


	public void setOtp(String otp) {
		this.otp = otp;
	}


	
	
	
	
	

}
