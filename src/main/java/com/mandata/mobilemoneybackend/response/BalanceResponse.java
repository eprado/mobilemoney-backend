package com.mandata.mobilemoneybackend.response;

import java.math.BigDecimal;

public class BalanceResponse extends ServiceResponse {

	private PocketResponse pocket;
	
	private BigDecimal totalBalance;

	public BalanceResponse() {
		super();
	}

	
	public BalanceResponse(PocketResponse pocket, BigDecimal totalBalance, String reponseCod, String responseDesc) {
		super(reponseCod, responseDesc);
		this.pocket = pocket;
		this.totalBalance = totalBalance;
	}


	public PocketResponse getPocket() {
		return pocket;
	}


	public void setPocket(PocketResponse pocket) {
		this.pocket = pocket;
	}


	public BigDecimal getTotalBalance() {
		return totalBalance;
	}


	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}


	@Override
	public String toString() {
		return super.toString() + "-->BalanceResponse [pocket=" + pocket.toString() + ", totalBalance=" + totalBalance + "]";
	}



	
}
