package com.mandata.mobilemoneybackend.response;

import java.util.List;

public class MyPocketsResponse extends ServiceResponse{
	
	List<PocketResponse> pocketsList;

	public MyPocketsResponse() {
		super();
	}


	public MyPocketsResponse(List<PocketResponse> pocketsList, String reponseCod, String responseDesc) {
		super(reponseCod, responseDesc);
		this.pocketsList = pocketsList;
	}


	public List<PocketResponse> getPocketsList() {
		return pocketsList;
	}


	public void setPocketsList(List<PocketResponse> pocketsList) {
		this.pocketsList = pocketsList;
	}


	@Override
	public String toString() {
		return super.toString() + "-->MyPocketsResponse [pocketsList=" + pocketsList + "]";
	}

}
