package com.mandata.mobilemoneybackend.response;

import java.util.ArrayList;
import java.util.List;

import com.mandata.mobilemoneybackend.entity.IdentificationType;

public class ParametersResponse extends ServiceResponse {
	
	private int bankCod;
	
	private String canal;
	
	private int bin;
	
	private int countryCod;
	
	private String applicationVersion;
	
	private int sessionTimeOut;
	
	private List<IdentificationType> identificationTypes = new ArrayList<>();

	public ParametersResponse() {
		super();
	}


	public ParametersResponse(int bankCod, String canal, int bin, int countryCod, String applicationVersion,
			int sessionTimeOut, List<IdentificationType> identificationTypes, String reponseCod, String responseDesc) {
		super(reponseCod, responseDesc);
		this.bankCod = bankCod;
		this.canal = canal;
		this.bin = bin;
		this.countryCod = countryCod;
		this.applicationVersion = applicationVersion;
		this.sessionTimeOut = sessionTimeOut;
		this.identificationTypes = identificationTypes;
	}


	public int getBankCod() {
		return bankCod;
	}


	public void setBankCod(int bankCod) {
		this.bankCod = bankCod;
	}


	public String getCanal() {
		return canal;
	}


	public void setCanal(String canal) {
		this.canal = canal;
	}


	public int getBin() {
		return bin;
	}


	public void setBin(int bin) {
		this.bin = bin;
	}


	public int getCountryCod() {
		return countryCod;
	}


	public void setCountryCod(int countryCod) {
		this.countryCod = countryCod;
	}


	public String getApplicationVersion() {
		return applicationVersion;
	}


	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}


	public int getSessionTimeOut() {
		return sessionTimeOut;
	}


	public void setSessionTimeOut(int sessionTimeOut) {
		this.sessionTimeOut = sessionTimeOut;
	}


	public List<IdentificationType> getIdentificationTypes() {
		return identificationTypes;
	}


	public void setIdentificationTypes(List<IdentificationType> identificationTypes) {
		this.identificationTypes = identificationTypes;
	}


	@Override
	public String toString() {
		return  super.toString()+ "--> ParametersResponse [bankCod=" + bankCod + ", canal=" + canal + ", bin=" + bin + ", countryCod="
				+ countryCod + ", applicationVersion=" + applicationVersion + ", sessionTimeOut=" + sessionTimeOut
				+ ", identificationTypes=" + identificationTypes + "]";
	}
	

}
