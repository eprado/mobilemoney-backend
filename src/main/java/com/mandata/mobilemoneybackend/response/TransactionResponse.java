package com.mandata.mobilemoneybackend.response;

import java.math.BigDecimal;

public class TransactionResponse extends ServiceResponse{
	
	private BigDecimal totalBalance;
	
	private BigDecimal pocketBalance;
	
	private String pocketState;

	
	
	public TransactionResponse() {
		super();
	}

	public TransactionResponse(BigDecimal totalBalance, BigDecimal pocketBalance, String pocketState, String reponseCod, String responseDesc) {
		super(reponseCod, responseDesc);
		this.totalBalance = totalBalance;
		this.pocketBalance = pocketBalance;
		this.pocketState = pocketState;
	}

	public BigDecimal getTotalBalance() {
		return totalBalance;
	}

	public void setTotalBalance(BigDecimal totalBalance) {
		this.totalBalance = totalBalance;
	}

	public BigDecimal getPocketBalance() {
		return pocketBalance;
	}

	public void setPocketBalance(BigDecimal pocketBalance) {
		this.pocketBalance = pocketBalance;
	}

	public String getPocketState() {
		return pocketState;
	}

	public void setPocketState(String pocketState) {
		this.pocketState = pocketState;
	}

	@Override
	public String toString() {
		return super.toString() + "-->TransactionResponse [totalBalance=" + totalBalance + ", pocketBalance=" + pocketBalance
				+ ", pocketState=" + pocketState + "]";
	}
	

}
