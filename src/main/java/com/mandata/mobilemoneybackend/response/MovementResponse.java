package com.mandata.mobilemoneybackend.response;

import java.math.BigDecimal;
import java.util.List;

import com.mandata.mobilemoneybackend.entity.TransactionalLog;

public class MovementResponse extends TransactionResponse{
	
	List<TransactionalLog> movementsList;

	public MovementResponse() {
		super();
	}

	public MovementResponse(BigDecimal totalBalance, BigDecimal pocketBalance, String pocketState, String reponseCod,
			String responseDesc, List<TransactionalLog> movementsList) {
		super(totalBalance, pocketBalance, pocketState, reponseCod, responseDesc);
		this.movementsList = movementsList;
	}

	
	public List<TransactionalLog> getMovementsList() {
		return movementsList;
	}

	public void setMovementsList(List<TransactionalLog> movementsList) {
		this.movementsList = movementsList;
	}

	@Override
	public String toString() {
		return super.toString() + "-->MovementResponse [movementsList=" + movementsList + "]";
	}
	
}
