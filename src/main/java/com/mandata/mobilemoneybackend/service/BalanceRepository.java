package com.mandata.mobilemoneybackend.service;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mandata.mobilemoneybackend.entity.Balance;

public interface BalanceRepository extends JpaRepository<Balance, Long> {

}
