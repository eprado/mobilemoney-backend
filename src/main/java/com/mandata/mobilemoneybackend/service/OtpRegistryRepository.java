package com.mandata.mobilemoneybackend.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mandata.mobilemoneybackend.entity.OtpRegistry;

@Transactional
public interface OtpRegistryRepository extends JpaRepository<OtpRegistry, Long>{
	
	 @Query(value="SELECT * FROM BMOT WHERE BOTBCO = :bankCod AND BOTCNL =:canal AND BOTTEL = :phoneNumber", nativeQuery=true)
	 List<OtpRegistry> findByBankAndPhone(@Param("bankCod") int bankCod, @Param("canal") String canal, @Param("phoneNumber") String phoneNumber);
	 
	 
	 @Modifying
	 @Query(value="UPDATE BMOT SET BOTOTP = :otp WHERE BOTIDE = :id", nativeQuery=true)
	 int updateOtp(@Param("id") int id, @Param("otp") String otp);
	 
	 
	 @Modifying
	 @Query(value="DELETE FROM BMOT WHERE BOTBCO = :bankCod and BOTCNL = :canal and BOTTEL = :phoneNumber", nativeQuery=true)
	 void deleteOtp(@Param("bankCod") int bankCod, @Param("canal") String canal, @Param("phoneNumber") String phoneNumber);
	 

}
