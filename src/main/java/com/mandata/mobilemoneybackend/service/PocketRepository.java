package com.mandata.mobilemoneybackend.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mandata.mobilemoneybackend.entity.Pocket;

public interface PocketRepository extends JpaRepository<Pocket, Long>{
	
	 @Query(value="SELECT * FROM BMBO WHERE BBOBCO = :bankCod AND BBOBIN =:bin AND BBOBOL =:pocketId AND BBOTEL = :phoneNumber LIMIT 1", nativeQuery=true)
	 Pocket findByPrimaryKey(@Param("bankCod") int bankCod, @Param("bin") long bin, @Param("pocketId") int pocketId, @Param("phoneNumber") String phoneNumber);

	 @Query(value="SELECT * FROM BMBO WHERE BBOBCO = :bankCod AND BBOBIN =:bin AND BBOTEL = :phoneNumber ORDER BY BBOBOL ASC", nativeQuery=true)
	 List<Pocket> findPocketList(@Param("bankCod") int bankCod, @Param("bin") long bin, @Param("phoneNumber") String phoneNumber);
	 
	 @Query(value="SELECT * FROM BMBO WHERE BBOBCO = :bankCod AND BBOBIN =:bin AND BBOTEL = :phoneNumber AND BBODEF = 'S' LIMIT 1", nativeQuery=true)
	 Pocket findDefault(@Param("bankCod") int bankCod, @Param("bin") long bin, @Param("phoneNumber") String phoneNumber);
	 
	 @Query(value="SELECT * FROM BMBO WHERE BBOBCO = :bankCod AND BBOTEL = :phoneNumber AND BBODEF = 'S' LIMIT 1", nativeQuery=true)
	 Pocket findDefaultByBankAndPhone(@Param("bankCod") int bankCod, @Param("phoneNumber") String phoneNumber);
	 
}
