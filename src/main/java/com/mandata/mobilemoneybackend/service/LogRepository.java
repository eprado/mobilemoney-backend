package com.mandata.mobilemoneybackend.service;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mandata.mobilemoneybackend.entity.Log;

@Transactional
public interface LogRepository extends JpaRepository<Log, Long> {

}
