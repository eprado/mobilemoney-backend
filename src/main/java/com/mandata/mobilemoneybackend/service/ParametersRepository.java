package com.mandata.mobilemoneybackend.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mandata.mobilemoneybackend.entity.Parameters;


public interface ParametersRepository extends JpaRepository<Parameters, Integer> {
	
	 @Query(value="SELECT * FROM BMPG WHERE BPGBCO = :bankCod and BPGCNL = :canal", nativeQuery=true)
	 List<Parameters> findByBankAndCanal(@Param("bankCod") int bankCod, @Param("canal") String canal);
	
	
}
