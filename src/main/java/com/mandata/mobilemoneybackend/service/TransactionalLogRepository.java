package com.mandata.mobilemoneybackend.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mandata.mobilemoneybackend.entity.TransactionalLog;

public interface TransactionalLogRepository extends JpaRepository<TransactionalLog, Long> {
	
	@Query(value="SELECT * FROM BMLM WHERE BLMBCO = :bankCod AND BLMBIN =:bin AND BLMPAI =:countryCod AND BLMTEL = :phoneNumber AND BLMBOL = :pocketId", nativeQuery=true)
	List<TransactionalLog> findByPocket(@Param("bankCod") int bankCod, @Param("bin") long bin, @Param("countryCod") int countryCod, @Param("phoneNumber") String phoneNumber, @Param("pocketId") int pocketId);

}
