package com.mandata.mobilemoneybackend.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mandata.mobilemoneybackend.entity.Master;

public interface MasterRepository extends JpaRepository<Master, Long>  {
	
	 @Query(value="SELECT * FROM BMMA WHERE BMABCO = :bankCod AND BMABIN =:bin and BMAPAI =:countryCod AND BMATEL = :phoneNumber LIMIT 1", nativeQuery=true)
	 Master findByPrimaryKey(@Param("bankCod") int bankCod, @Param("bin") long bin, @Param("countryCod") int countryCod, @Param("phoneNumber") String phoneNumber);
	 
	 @Query(value="SELECT * FROM BMMA WHERE BMABCO = :bankCod AND BMABIN =:bin and BMAPAI =:countryCod AND BMATID = :idType AND BMANID =:idNumber LIMIT 1", nativeQuery=true)
	 Master findByIdentification(@Param("bankCod") int bankCod, @Param("bin") long bin, @Param("countryCod") int countryCod, @Param("idType") String idType, @Param("idNumber") String idNumber);
	 
}
