package com.mandata.mobilemoneybackend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class DuplicatedResourseException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public DuplicatedResourseException(String message) {
		super(message);
	}

}
