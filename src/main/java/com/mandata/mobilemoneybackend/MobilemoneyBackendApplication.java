package com.mandata.mobilemoneybackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MobilemoneyBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(MobilemoneyBackendApplication.class, args);
	}
}
